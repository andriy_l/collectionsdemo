package poligon.lab1;

import java.util.*;
import java.util.function.Consumer;
class Apple implements Comparable<Apple>{
    private String type;

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Apple{" +
                "type='" + type + '\'' +
                '}';
    }

    public Apple(String type) {
        this.type = type;
    }

    @Override
    public int compareTo(Apple o) {
        return this.type.compareTo(o.type);
    }
}
public class QueueDemo {
    public static void main(String[] args) {
        Apple a1 = new Apple("fuji");
        Apple a2 = new Apple("golden");
        Apple a3 = new Apple("liza");
        Apple a4 = new Apple("ajdaret");
        Apple a5 = new Apple("ligold");
        Apple a6 = new Apple("honey_crisp");
        Set<Apple> queue = new TreeSet<>();
        // HashTable, Vector, Stack, BitSet
        // Enumeration e; since java 1.0, before java 1.2 (java 2)
        queue.add(a4);
        queue.add(a3);
        queue.add(a2);
        queue.add(a1);
        queue.add(a3);

        System.out.println(queue);
//        queue.forEach( s -> System.out.println(s+" "+s.hashCode()) );

//        queue.forEach((String s) -> {System.out.println(s.hashCode());} );
//
//        queue.forEach(new Consumer<String>() {
//            @Override
//            public void accept(String s) {
//                System.out.println(s.hashCode());
//            }
//        });


    }
}
