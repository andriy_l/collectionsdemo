package poligon.lab1;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

public class MyNumGenerator {
    public static Set<Integer> generateDistinct(int numOfElm, int maxNumb){
        Set<Integer> generatatedValues = new TreeSet<>();
        Random random = new Random();
        do{
            generatatedValues.add(random.nextInt(maxNumb));
        }while (generatatedValues.size() < numOfElm);
        return generatatedValues;
    }

    public static void main(String[] args) {
        System.out.println(generateDistinct(5, 64));
    }
}
