package poligon.lab1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by andriy on 10.06.17.
 */
public class MyTranslator {
    static HashMap<String,ArrayList<String>> dictionary = new HashMap<>();

    public static void addNewWord(String en, String ua){
        if(dictionary.containsKey(en)){
            ArrayList<String> words = dictionary.get(en);
            words.add(ua);
            dictionary.put(en, words);
            return;
        }
        ArrayList<String> words = new ArrayList<String>();
        words.add(ua);
            dictionary.put(en, words);
    }

    public static ArrayList<String> translate(String en){
        return dictionary.get(en);
    }

    public static void main(String[] args) {
        addNewWord("cat", "кіт");
        System.out.println(dictionary);
        addNewWord("dog", "пес");
        System.out.println(dictionary);
        addNewWord("hedgehog", "їжачок");
        System.out.println(dictionary);
        addNewWord("hedgehog", "їжачиха");
        addNewWord("and", "i");
        addNewWord("are", "є");
        System.out.println(dictionary);
        String str = "hedgehog and cat are friends";
        for(String s : str.split(" ")){
            System.out.print(translate(s) + " ");
        }
        List<String> list = Arrays.asList("one","two","three");

    }
}
