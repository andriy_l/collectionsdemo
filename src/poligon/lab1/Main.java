package poligon.lab1;

import javax.swing.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<String> list1 = new LinkedList<>();
        List<String> list2 = new ArrayList<>();
        ArrayList<String> list3 = new ArrayList<>();
        Set<String> set   = new HashSet<>();
        for (int i = 0, j = 9; i < 10; i++, j--) {
            list1.add("number_" + i);
            list2.add(new String("number_" + j));
            list3.add(new String("number_" + j));
            set.add("number_" + i);
            set.add("number_" + j);
        }
        list3.trimToSize();
        System.out.println(list1);
        System.out.println(list2);
        System.out.println();
        System.out.println(list3.subList(3,6));
        System.out.println(list1.equals(list2));
        System.out.println(list3.equals(list2));



    }
}
