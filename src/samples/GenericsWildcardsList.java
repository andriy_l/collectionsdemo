package samples;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by andriy on 1/30/17.
 */
class A{}
class B extends A{}
class C extends B{}

public class GenericsWildcardsList {
    static ArrayList<A> list = new ArrayList<>();


    public static void main(String[] args) {
        addToList(list);
        addToList(list);
        addToList(list);
        addToList(list);
    }

    public static void addToList(List<A> myList){
        myList.add(new B());
        myList.add(new A());
        myList.add(new C());
        // myList.add(new Object());
        myList.remove(2);
    }

    /*
    !!! В метод можна буде передавати колекції параметризовані будь-яким допустимим типом, а саме класом A
    і будь-яким його підкласом, що є неможливо без використання анонімного символа "?"
    Але в методі неможливо буде додати новий елемент, навіть допустимого типу, оскільки, заздалегідь компілятору
     невідомий тип параметризації списку
     */
    public static void cannotAddToList(List<? extends A> myList){
     //   myList.add(new B()); // додавання до списків, які параметризовані метасимволом з
    //    myList.add(new A()); // використанням extends заборонено завжди!!!!
    //    myList.add(new C());
    //    myList.add(new Object());
        myList.remove(1);
    }

    /*
    параметр методу або значення, що повертається може отримати список типу A або будь-якого з його суперкласів,
    в той же час дозволяє додавати туди екземпляри класу A і будь-яких його підкласів
     */
    public static void canAddToList(List<? super A> myList){
           myList.add(new B());
           myList.add(new A());
           myList.add(new C());
        //   myList.add(new Object());
        myList.remove(1);
    }


}
