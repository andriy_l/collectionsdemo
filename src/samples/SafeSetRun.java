package samples;


import java.util.*;

class Bird{
    private String name;
    Bird(String name){
        this.name = name;
    }
}
class Crockodile{
    private String name;
    Crockodile(String name){
        this.name = name;
    }
}
public class SafeSetRun {
    public static void main(String args[ ]) {

        Set birds;
// orders = new HashSet(); // lower than jdk1.5
        birds = Collections.checkedSet(new HashSet<Bird>(), Bird.class);
        birds.add(new Bird("rooster"));
// some code here
        birds.add(new Crockodile("Gena")); // runtime error

    }
}
