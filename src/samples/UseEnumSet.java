package samples;
import java.util.EnumSet;

enum CarManufacturer {
    AUDI, BMW, VW, TOYOTA, HONDA, ISUZU, SUZUKI, VOLVO, RENAULT
}

public class UseEnumSet {
    public static void main(String[ ] args) {

/* множина japanAuto містить елементи типу
enum з інтервалу, який визначений двома елементами */

        EnumSet <CarManufacturer> japanAuto =
                EnumSet.range(CarManufacturer.TOYOTA, CarManufacturer.SUZUKI);
/* множина other буде містити всі елементи яких нема в множині japanAuto */
        EnumSet <CarManufacturer> other = EnumSet.complementOf(japanAuto);
        System.out.println(japanAuto);
        System.out.println(other);
        action("audi", japanAuto);
        action("suzuki", japanAuto);
    }
    public static boolean action(String auto, EnumSet <CarManufacturer> set) {

        CarManufacturer cm = CarManufacturer.valueOf(auto.toUpperCase());
        boolean ok = false;
        if(ok = set.contains(cm)) {
// processing
            System.out.println("Processed: " + cm);
        } else {
            System.out.println("Processed impossible: " + cm);
        }
        return ok;
    }
}