package samples;

import java.util.*;

/**
 * Created by andriy on 1/30/17.
 */
public class DemoHashMap {
    public static void main(String[] args) {

        HashMap<String, Integer> hm = new HashMap<String, Integer>(3) {
            {
                this.put("Hedgehog", 3);
                this.put("Rooster", 5);
                this.put("Cat", 1);
                this.put("Dog", 1);

            }
        };

        if(!hm.containsKey("Rooster")) {
            hm.put("Rooster", 4); // same key!
        }

        System.out.println(hm);
        hm.put("Chicken", 4); // заміна або додавання при відсутності ключа
        System.out.println(hm + "after change element");
        Integer a = hm.get("Dog");
        System.out.println(a + " - found by key 'Dog'");
// вивід хеш-таблиці за допомогою методів інтерфейсу Map.Entry<K,V>
        Set<Map.Entry<String, Integer>> setv = hm.entrySet();
        System.out.println(setv);
        Iterator<Map.Entry<String, Integer>> i = setv.iterator();
        while (i.hasNext()) {
            Map.Entry<String, Integer> me = i.next();
            System.out.println(me.getKey() + " : " + me.getValue());
        }
        Set<Integer> val = new HashSet<Integer>(hm.values());
        System.out.println(val);
    }
}