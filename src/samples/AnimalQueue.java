package samples;
import java.util.LinkedList;
import java.util.Queue;

class Animal{
    private String name;
    private long id;

    public String getName() {
        return name;
    }

    public long getId() {
        return id;
    }

    public Animal(String name, long id) {
        this.name = name;
        this.id = id;
    }
}
public class AnimalQueue {
    public static void main(String[ ] args) {

        LinkedList<Animal> animals = new LinkedList<Animal>() {
            {
                add(new Animal("Wolfy", 5l));
                add(new Animal("Lion", 4L));
                add(new Animal("Parrot", 1L));
            }
        };
        Queue<Animal> queueA = animals; // створення черги
        queueA.offer(new Animal("Crocodile", 9l));
        orderProcessing(queueA); // обробка черги
        if (queueA.isEmpty()) {
            System.out.println("Queue of Orders is empty");
        }
    }
    public static void orderProcessing(Queue<Animal> queue) { // можна замінити void -> boolean

        Animal ob = null;
        // можна замінити while -> do{} while
        while ((ob = queue.poll()) != null) { // отримання з видаленням
            System.out.println("Animal #" + ob.getId() + " is " + ob.getName());
            // verifying and processing
        }
    }
}