package samples;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andriy on 5/14/17.
 */
public class ListWithoutGenericsDemoQuestion {
    public static void main(String[] args) {
        // What will be the output?
        List longs = new ArrayList<Long>();
        longs.add(1L);
        longs.add(1.0);
        longs.add(new Object());
        longs.add("I am LONG!");

        System.out.println(longs.size());

        // #1 3
        // #2 4  <= correct
        // #3 Compile error
        // #4 Runtime exception

        // information about Generics will be erased after compile-time
        // and List will be not strictly typed List
        // adding these values to List at Runtime: it is possible to add them
    }
}
