package samples;

import java.util.Set;
import java.util.TreeSet;

/**
 * Created by andriy on 5/14/17.
 */
public class InsertIntoTreeSetWithCastingDemoQuestion {
    public static void main(String[] args) {

        // What will be the output?
        Set<Number> set = new TreeSet<Number>();
        set.add(1);
        set.add(1L);
        set.add(1.0);

        System.out.println(set.size());

        // #1  1
        // #2  2
        // #3  3
        // #4  ClassCastException  <= correct
        // #5  CompileError

        // TreeSet call Comparator.compareTo() and cannot compare  Integer and Long
        // ClassCastException: java.lang.Integer cannot be cast to java.lang.Long
    }
}
