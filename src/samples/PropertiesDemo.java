package samples;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
public class PropertiesDemo {
    public static void main(String[ ] args) {
        Properties props = new Properties();
        try {
// завантаження пар ключ-значення через потік введення-виведення
            props.load(new FileReader("text"+ File.separator+"database.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String driver = props.getProperty("db.driver");
// снаступних двох ключів у файлі немає
        String maxIdle = props.getProperty("db.maxIdle"); // буде присвоенo null
// значення "20" буде присвоєно ключу, якщо він не буде знайдений в файлі
        String maxActive = props.getProperty("db.maxActive", "20");
        System.out.println(driver);
        System.out.println(maxIdle);
        System.out.println(maxActive);
    }
}
