package samples;

import java.util.*;
class Dog {
    int size;
    Dog(int s) {
        size = s;
    }
}
public class CollectionsSamples {

    public static void main(String []args) {
        HashMap props = new HashMap();
        props.put("key45", "some value");
        props.put("key12", "some other value");
        props.put("key39", "yet another value");
        Set s = props.keySet();
        System.out.println(s);
        s = new TreeSet(s);
        System.out.println(s);
    }

}
